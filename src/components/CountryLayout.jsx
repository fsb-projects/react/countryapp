import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import {Link} from 'react-router-dom'
import LanguagesList from './LanguagesList';

const useStyles = makeStyles(theme => ({

  card: {
    maxWidth: "80%",
    margin:"10px auto",
  },
  avatar: {
    backgroundColor: red[500],
  }
}));

//the {children} inside CardContent component will be used by CurrentCountry container.

const CountryLayout= ({code,name,continent,native,languages,children})=>{
    const classes = useStyles();
    return (
        <Card className={classes.card}>
            <CardHeader
              avatar={<Avatar 
                        aria-label="recipe" className={classes.avatar}>
                        {code}
                      </Avatar>}
              title={<Typography 
                      variant="h4" component={Link} 
                      to={`/countries/${code}`} 
                      color="textSecondary">
                          {`${name} (${continent.name})`}
                      </Typography>}
              subheader={<Typography  
                          variant="h6" color="textSecondary">
                          {native}
                        </Typography>}
            />
            <CardContent>
                {children}
                <LanguagesList languages={languages}/>
            </CardContent>
      </Card>
      )
} 

CountryLayout.propTypes={
  code:PropTypes.string,
  name:PropTypes.string,
  continent:PropTypes.objectOf(
    PropTypes.string
  ),
  native:PropTypes.string,
  languages:PropTypes.array,
  children:PropTypes.element
}

export default CountryLayout