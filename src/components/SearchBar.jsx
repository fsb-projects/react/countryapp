import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    margin:"0 auto",
    width:"80%"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  }
}));


const SearchBar= ({nameFilter,handleChange})=>{
  const classes = useStyles();

  return (
    <form className={classes.container} noValidate autoComplete="off">
      <TextField
        onChange={handleChange}
        value={nameFilter}
        id="standard-search"
        label="Search field"
        type="search"
        className={classes.textField}
        margin="normal"
      />
    </form>
  );
}

SearchBar.propTypes={
  nameFilter:PropTypes.string,
  handleChange:PropTypes.func
}

export default SearchBar
